package ru.volkova.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.volkova.tm.entity.Task;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

public interface ITaskRepository extends IOwnerRepository<Task> {

    @NotNull
    Optional<Task> add(
            @NotNull String userId,
            @NotNull String name,
            @NotNull String description
    );

    void bindTaskByProjectId(
            @NotNull String userId,
            @NotNull String projectId,
            @NotNull String taskId
    );

    @NotNull
    List<Task> findAllByProjectId(@NotNull String userId,@NotNull String projectId);

    void removeAllByProjectId(@NotNull String userId,@NotNull String projectId);

    void unbindTaskByProjectId(
            @NotNull String userId,
            @NotNull String projectId,
            @NotNull String taskId
    );

}
