package ru.volkova.tm.command.system;

import com.jcabi.manifests.Manifests;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.command.AbstractCommand;

public class VersionCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "show application version";
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println(Manifests.read("build"));

    }

    @NotNull
    @Override
    public String name() {
        return "version";
    }

}
