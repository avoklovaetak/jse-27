package ru.volkova.tm.dto;

import lombok.Setter;
import lombok.Getter;
import ru.volkova.tm.entity.Project;
import ru.volkova.tm.entity.Task;
import ru.volkova.tm.entity.User;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class Domain implements Serializable {

    private List<Project> projects;

    private List<Task> tasks;

    private List<User> users;

}
