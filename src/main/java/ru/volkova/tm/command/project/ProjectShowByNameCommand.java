package ru.volkova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.enumerated.Role;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;
import ru.volkova.tm.exception.entity.ProjectNotFoundException;
import ru.volkova.tm.entity.Project;
import ru.volkova.tm.util.TerminalUtil;

import java.util.Optional;

public class ProjectShowByNameCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "show project by name";
    }

    @Override
    public void execute() {
        if (serviceLocator == null) throw new ObjectNotFoundException();
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER NAME:");
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        @Nullable final String name = TerminalUtil.nextLine();
        @NotNull final Optional<Project> project = serviceLocator.getProjectService().findOneByName(userId, name);
        if (!project.isPresent()) throw new ProjectNotFoundException();
        showProject(project.get());
    }

    @NotNull
    @Override
    public String name() {
        return "project-show-by-name";
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
