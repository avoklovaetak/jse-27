package ru.volkova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.enumerated.Role;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;
import ru.volkova.tm.util.TerminalUtil;

public class UserByIdLockCommand extends AbstractUserCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "lock user by id";
    }

    @Override
    public void execute() {
        if (serviceLocator == null) throw new ObjectNotFoundException();
        System.out.println("[LOCK USER]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        serviceLocator.getUserService().lockById(id);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public String name() {
        return "user-lock-by-id";
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
