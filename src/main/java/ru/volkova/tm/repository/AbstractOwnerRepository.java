package ru.volkova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.repository.IOwnerRepository;
import ru.volkova.tm.entity.AbstractOwnerEntity;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public abstract class AbstractOwnerRepository<E extends AbstractOwnerEntity>
        extends AbstractRepository<E> implements IOwnerRepository<E> {

    @Override
    public void clear(@NotNull final String userId) {
        entities.clear();
    }

    @NotNull
    @Override
    public List<E> findAll(@NotNull final String userId) {
        return entities.stream()
                .filter(predicateByUserId(userId))
                .collect(Collectors.toList());
    }
    @NotNull
    @Override
    public List<E> findAll(@NotNull final String userId,@NotNull final Comparator<E> comparator) {
        return entities.stream()
                .filter(predicateByUserId(userId))
                .sorted(comparator)
                .collect(Collectors.toList());

    }

    @NotNull
    @Override
    public Optional<E> findById(@NotNull final String userId,@NotNull final String id) {
        return Optional.ofNullable((entities.stream()
                .filter(predicateByIdAndUserId(id, userId))
                .findFirst()
                .orElseThrow(ObjectNotFoundException::new)));
    }

    @NotNull
    @Override
    public Optional<E> findOneByIndex(@NotNull final String userId,@NotNull final Integer index) {
        return entities.stream()
                .filter(predicateByUserId(userId))
                .skip(index)
                .findFirst();
    }

    @NotNull
    @Override
    public Optional<E> findOneByName(@NotNull final String userId,@NotNull final String name) {
        return entities.stream()
                .filter(predicateByUserIdAndName(userId, name))
                .findFirst();
    }

    @NotNull
    protected final Predicate<E> predicateByIdAndUserId(final String id, final String userId) {
        return e -> id.equals(e.getId()) && userId.equals(e.getUserId());
    }

    @NotNull
    protected final Predicate<E> predicateByUserId(final String userId) {
        return e -> userId.equals(e.getUserId());
    }

    @NotNull
    protected final Predicate<E> predicateByUserIdAndName(final String userId, final String name) {
        return e -> name.equals(e.getName()) && userId.equals(e.getUserId());
    }

    @Override
    public void removeById(@NotNull final String userId,@NotNull final String id) {
        findById(userId, id).ifPresent(this::remove);
    }

    @Override
    public void removeOneByIndex(@NotNull final String userId,@NotNull final Integer index) {
        findOneByIndex(userId, index).ifPresent(this::remove);
    }

    @Override
    public void removeOneByName(final @NotNull String userId, final String name) {
        findOneByName(userId, name).ifPresent(this::remove);
    }

}
