package ru.volkova.tm.command.bonds;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.entity.Task;
import ru.volkova.tm.enumerated.Role;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;
import ru.volkova.tm.util.TerminalUtil;

import java.util.List;

public class TasksShowByProjectIdCommand extends AbstractProjectTaskClass {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "show all tasks of project by project id";
    }

    @Override
    public void execute() {
        if (serviceLocator == null) throw new ObjectNotFoundException();
        System.out.println("[TASK LIST OF PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        @NotNull final List<Task> tasks = serviceLocator.getProjectTaskService()
                .findAllTasksByProjectId(userId, projectId);
        int index = 1;
        for (final Task task : tasks) {
            System.out.println((index + ". " + task));
            index++;
        }
    }

    @NotNull
    @Override
    public String name() {
        return "tasks-show-by-project-id";
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}

