package ru.volkova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.repository.ICommandRepository;
import ru.volkova.tm.command.AbstractCommand;

import java.util.*;

public class CommandRepository implements ICommandRepository {

    @NotNull
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @NotNull
    private final Map<String, AbstractCommand> arguments = new LinkedHashMap<>();

    @Override
    public void add(@NotNull AbstractCommand command) {
        @Nullable final String arg = command.arg();
        @Nullable final String name = command.name();
        if (arg != null) arguments.put(arg, command);
        if (name != null) commands.put(name, command);
    }

    @NotNull
    public Collection<AbstractCommand> getArguments() {
        return arguments.values();
    }

    @NotNull
    public AbstractCommand getCommandByArg(@NotNull String name) {
        return commands.get(name);
    }

    @NotNull
    public AbstractCommand getCommandByName(@NotNull String name) {
        return commands.get(name);
    }

    @NotNull
    public Collection<AbstractCommand> getCommands() {
        return commands.values();
    }

}
